parseSEDFitting <- function(params, 
                            fit_ff, 
                            model, 
                            objid, 
                            flux_6cm, 
                            points_no, 
                            sed_quality_old,
                            sed_quality_new
                            ) {
    
    curr_time <- format(as.POSIXlt(Sys.time()), '%Y-%m-%d %H:%M:%S')

    ## if known distance claculate physical diameter
    use_Diam <- NA
    if (length(params$fit$distance$D) == 1) {
        use_Diam <- PhDiam(coef(fit_ff)[[2]], params$fit$distance$D * 1000)
    }
    
    ## if fit quality == 0 convergence is assigned to False
    isconverged <- fit_ff$convInfo$isConv | model$mode == "linear"
    
    ## prepare fitset for record to mysql
    fitset <- data.frame(
        idPNMain = objid,
        crfreq = coef(fit_ff)[[1]],
        model = model$model,
        funct = as.character(model$func),
        ni = model$ni,
        D = params$fit$distance$D,
        errD_up = params$fit$distance$errD_up,
        errD_down = params$fit$distance$errD_down,
        method = params$fit$distance$method,
        tbl = params$fit$distance$tbl,
        Diam = use_Diam,
        morph = factor(params$fit$morphology),
        Te = params$fit$Te,
        EM = EM(coef(fit_ff)[[1]], params$fit$Te),
        theta = coef(fit_ff)[[2]],
        init_diam = params$fit$init_diam,
        opt_diam = params$fit$diam,
        n_points = points_no,
        isConverged = isconverged,
        flux_6cm_est = flux_6cm,
        time_in = curr_time
    )

    fit.params.all <- data.frame(
        idPNMain = objid,
        model = model$model,
        fittinglib = fit_ff$fittingLib,
        Te = params$fit$Te,
        n_points = points_no,
        isConv = isconverged,
        finIter = fit_ff$convInfo$finIter,
        finTol = fit_ff$convInfo$finTol,
        nrmse = sed_quality_old$nrmse,
        mse = sed_quality_old$mse,
        fit_quality_old = sed_quality_old$fit_qual,
        fit_quality_new = sed_quality_new$fit_qual,
        time_in = curr_time
    )

    if (model$model == "linear") {
        fit_params.model <- data.frame(
            lin_inters = 10 ^ fit_ff$coefficients[[1]],
            lin_slope = 10 ^ fit_ff$coefficients[[2]]
        )
    } else {
        fit_params.model <- data.frame(
            ni = model$ni,
            theta_start = fit_ff$call$start[["theta"]],
            theta_low_lim = fit_ff$call$lower[["theta"]],
            theta_up_lim = fit_ff$call$upper[["theta"]],
            cfreq_start = fit_ff$call$start[["freq_0"]],
            cfreq_low_lim = fit_ff$call$lower[["freq_0"]],
            cfreq_up_lim = fit_ff$call$upper[["freq_0"]],
            stopCode = fit_ff$convInfo$stopCode,
            stopMessage = fit_ff$convInfo$stopMessage,
            theta = coef(fit_ff)[[2]],
            theta_std_err = summary(fit_ff)$parameters[2, 2],
            theta_t_val = summary(fit_ff)$parameters[2, 3],
            theta_p_val = summary(fit_ff)$parameters[2, 4],
            cfreq = coef(fit_ff)[[1]],
            cfreq_std_err = summary(fit_ff)$parameters[1, 2],
            cfreq_t_val = summary(fit_ff)$parameters[1, 3],
            cfreq_p_val = summary(fit_ff)$parameters[1, 4]
        )
    }

    # fit_params <- cbind(fit.params.all, fit_params.model)
    list('fitset' = fitset, 
         'fit.params' = cbind(fit.params.all, fit_params.model))
}
