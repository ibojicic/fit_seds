library(nls2)

source('R/db/loadData.R')
source('R/db/recNewDBResults.R')
source('R/utilities.R')
source('R/fit/fitSEDModels.R')
source('R/plot/plotModels.R')
################################
## initialise input parametes
## the values are input from R/params/params_*.R files
params = list()
source('R/params/params_path.R')
source('R/params/params_plot.R')
source('R/params/params_db.R')
################################

objid = 6624

## select points for fittinf
obj.meta <- meta.data[meta.data$idPNMain == objid,]

inputData <- filter(data.parsed, idPNMain == objid)
inputData <- filter(inputData, InUse %in% c('y')) 

obj.data <- inputSED(inputData, obj.meta)


## load input parameters
source('R/params/params_fit.R')


final_results <- fitSEDModels(obj.data, params)

final_plot <- plotModels(obj.data,final_results,params)

recNewDBResults(params, final_results)
